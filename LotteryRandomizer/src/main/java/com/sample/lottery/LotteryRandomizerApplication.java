package com.sample.lottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LotteryRandomizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LotteryRandomizerApplication.class, args);
	}

}
