package com.sample.lottery;

import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RandomController {
	Random randomNum = new Random();
	
	@GetMapping("/lottery/random")
	public String getRandomNumber() {
		return randomNum.nextInt(100) +"";
	}
}