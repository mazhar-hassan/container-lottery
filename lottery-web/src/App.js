import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      winner: {"message":"wait"}
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/lottery/winner")
      .then(res => res.json())
      .then(
        (result) => {
      console.log(result)
          this.setState({
            isLoaded: true,
            winner: result
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          The <code>Lottery</code> App.
        </p>
        
          {this.state.winner.message}
        
      </header>
    </div>
  );
  }
}

export default App;
