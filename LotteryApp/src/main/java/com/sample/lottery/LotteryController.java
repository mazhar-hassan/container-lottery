package com.sample.lottery;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@CrossOrigin(maxAge = 3600)
@RestController
public class LotteryController {
	@Value("${api.url}")
    private String apiUrl;
	
	@GetMapping("/lottery/winner")
	public Winner getWinner() {
		Winner winner = new Winner();
		winner.setMessage("The Lucky winner of 1000 is "+getLuckNumber());
		return winner;
	}
	
	public int getLuckNumber() {
		RestTemplate rest = new RestTemplate();
		String result = rest.getForObject(apiUrl+"/lottery/random", String.class);
		return Integer.valueOf(result);
	}
}
